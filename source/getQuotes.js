import $ from 'jquery'
import template from './template.js'


const URL = 'https://quotesondesign.com/wp-json/posts?filter[orderby]=rand&filter[posts_per_page]=3'


export default (handler) => {
  $.getJSON(URL + '&' + Math.random(), (quotes) => {
    let html = ''

    for (let quote of quotes) {
      html += template(quote)
    }

    handler(html)
  })
}
