import $ from 'jquery'
import getQuotes from './getQuotes.js'


$(function () {
  const $quoteBox = $('#quote_box')
  const $quoteReload = $('#quote_reload')

  const loadQuotes = () => {
    $quoteReload.hide()
    $quoteBox.html('').addClass('loading')

    getQuotes((html) => {
      $quoteBox.removeClass('loading').html(html)
      $quoteReload.show()
    })
  }

  $quoteReload.on('click', loadQuotes)

  loadQuotes()
})
