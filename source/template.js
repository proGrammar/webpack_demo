export default (data) => (`
  <blockquote>
    <p>${ data.content }</p>
    <a href="${ data.link }" target="_blank">
      <cite>${ data.title }</cite>
    </a>
  </blockquote>
`)
