const { join } = require('path')
const PATH = process.cwd()
const SOURCE = join(PATH, 'source')
const PUBLIC = join(PATH, 'public')


// https://webpack.js.org/configuration/
module.exports = {
  context: SOURCE,
  entry: "./index.js",
  output: {
    path: PUBLIC,
    filename: 'bundle.js',
  },
  module: {
    loaders: [{
      test: /\.js$/,
      include: [SOURCE],
      loader: 'babel-loader',
      // https://babeljs.io/docs/plugins/preset-latest/
      options: {
        presets: ["es2015"]
      },
    }],
  },
 resolve: {
   modules: [
     'node_modules',
     SOURCE,
   ],
 },
 // https://webpack.js.org/configuration/dev-server/
 devServer: {
   contentBase: PUBLIC,
 },
}
