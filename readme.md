### Getting started


Clone the repository:

```
  $ git clone https://gitlab.com/ugin/webpack_demo.git
```

Or download the `.zip` from [gitlab.com/ugin/webpack_demo](https://gitlab.com/ugin/webpack_demo/repository/archive.zip?ref=master).

Install JavaScript modules from npm:

```
  $ cd webpack_demo
  $ npm install
```

Run the development server:

```
  $ npm start
```

Open the link `http://localhost:3000/webpack-dev-server/`.

The server will auto compile the code from `/source` and reload the browser.


#### Software used:

- [Node.js](https://nodejs.org/en/)
- [webpack](https://webpack.js.org/)
- [Babel](https://babeljs.io/)
- [Spectre CSS](https://picturepan2.github.io/spectre/)
- [now](https://zeit.co/now)

---

> License [ISC](https://gitlab.com/ugin/webpack_demo/blob/master/license) &nbsp;&middot;&nbsp;
> GitHub [@navaru](https://github.com/navaru) &nbsp;&middot;&nbsp;
> Twitter [@navaru](https://twitter.com/navaru)
